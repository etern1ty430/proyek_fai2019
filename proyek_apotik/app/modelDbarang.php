<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modelDbarang extends Model
{
    protected $table = "d_barang";
    protected $primaryKey = "id_dbarang";
    public $timestamps = false;
    protected $connection = "mysql";
    protected $fillable = ["id_dbarang", "id_barang", "stock", "expired", "jenis"];
}
