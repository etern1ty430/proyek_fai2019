<?php

namespace App\Http\Middleware;

use Closure;

class cekjabatan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$jabatan)
    {
        $loginsebagai = "";
        if($request->session()->get('acc_type', 'default') == 'E'){
            $loginsebagai = 'E';
        }
        else if($request->session()->get('acc_type', 'default') == 'C'){
            $loginsebagai = 'C';
        }

        foreach($jabatan as $job){
            if($job == $loginsebagai) return $next($request);
        }
        return redirect('proyek/login');
    }
}
