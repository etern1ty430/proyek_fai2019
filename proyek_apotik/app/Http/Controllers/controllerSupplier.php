<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class controllerSupplier extends Controller
{
    public function openaddsupplier(Request $request)
    {
        if($request->input("stock") == true){
            $getSupplier = DB::table('supplier')
            ->select("id_supplier")
            ->get();

            $data = [];
            $data["supplier"] = $getSupplier;

            return view("supplieradd_barang", $data);
        }
        else if($request->input("submit")){
            $name = $request->input("name");
            $address = $request->input("address");
            $phone = $request->input("phone");

            if(!$name=="" && !$address=="" && !$phone==""){
                //generate id_supplier
                $accSupplier = DB::table('supplier')
                ->max("id_supplier");
                $accSupplier = substr($accSupplier,2);
                $accSupplier = $accSupplier+1;
                $accSupplier = str_pad($accSupplier,3,"0",STR_PAD_LEFT);
                $accSupplier = "SU".$accSupplier;

                $arr = [
                    "id_supplier" => $accSupplier,
                    "nama_supplier" => $name,
                    "alamat" => $address,
                    "notelp" => $phone
                ];
                DB::table('supplier')->insert($arr);

                $request->session()->flash('error', 'Success Insert');
                return view('supplieradd');
            }
            else {
                $request->session()->flash('error', 'All Fields Must be Filled');
                return view('supplieradd');
            }
        }
    }

    public function openaddsupplier_barang(Request $request)
    {
        if($request->input('add')){
            $name = $request->input("name");
            $idsupplier = $request->input("idsupplier");
            $fungsi = $request->input("fungsi");
            $sPrice = $request->input("sPrice");
            $pPrice = $request->input("pPrice");

            if(!$name=="" && !$fungsi=="" && !$sPrice=="" && !$pPrice==""){

                $request->validate(
                    ['uploadkan'=> "required|mimes:jpg,png,jpeg|max:2048"]
                );
                $file = $request->file("uploadkan");
                //$filename = $file->getClientOriginalName(). ".". $file->getClientOriginalExtension();
                $filename = $file->getClientOriginalName();
                $file->move('img', $filename);

                $idbarang = "";
                $idbarang = DB::table('barang')
                ->max("id_barang");
                $idbarang = substr($idbarang,2);
                $idbarang = $idbarang+1;
                $idbarang = str_pad($idbarang,3,"0",STR_PAD_LEFT);
                $idbarang = "BA".$idbarang;

                $arrBarang = [
                    "id_barang" => $idbarang,
                    "nama_barang" => $name,
                    "fungsi" => $fungsi,
                    "harga" => $pPrice,
                    "img_resource" => $filename
                ];
                DB::table('barang')->insert($arrBarang);

                $arr = [
                    "id_barangsupplier" => null,
                    "id_supplier" => $idsupplier,
                    "id_barang" => $idbarang,
                    "harga" => $sPrice
                ];
                DB::table('barang_supplier')->insert($arr);

                $getSupplier = DB::table('supplier')
                ->select("id_supplier")
                ->get();

                $data = [];
                $data["supplier"] = $getSupplier;
                $request->session()->flash('error', 'Insert Barang Success');
                return view('supplieradd_barang', $data);
            }
            else {
                $getSupplier = DB::table('supplier')
                ->select("id_supplier")
                ->get();

                $data = [];
                $data["supplier"] = $getSupplier;
                $request->session()->flash('error', 'All Fields Must be Filled');
                return view('supplieradd_barang', $data);
            }
        }
        else if($request->input("supplier") == true){
            return redirect("supplier/pagesupplier");
        }
    }
}
