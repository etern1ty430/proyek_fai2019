<?php

namespace App\Http\Controllers;

use App\Models\d_beliModel;
use App\Models\d_jualModel;
use App\Models\h_beliModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class controllerLaporan extends Controller
{
    public function openLaporan(Request $request)
    {
        $tglAwal = $request->input('tglAwal');
        $tglSampai = $request->input('tglSampai');
        $filter = $request->input('cbFilter');

        if(!isset($tglAwal)) $tglAwal = "2000-01-01";
        if($tglSampai == null) $tglSampai = "2020-01-01";

        if($filter == "Pembelian") {
            $filterHeader = "h_beli";
            $filterDetail = "d_beli";
            $semuanota = d_beliModel::all();
        }
        else if($filter == "Penjualan") {
            $filterHeader = "h_jual";
            $filterDetail = "d_jual";
            $semuanota = d_jualModel::all();
        }
        $tempHbeli = [];
        // $tempHbeli = $model::whereDate("tanggal",">=", $tglAwal)
        // ->whereDate("tanggal", "<=", $tglSampai)
        // ->get();
        $tempHbeli = DB::table($filterHeader)
        ->select("nota_beli")
        ->whereDate("tanggal",">=", $tglAwal)
        ->whereDate("tanggal", "<=", $tglSampai)
        ->get();

        $data = [
            "d_beli"=>$semuanota,
            "h_beli" => $tempHbeli,
            "filter" => $filter
        ];

        return view('laporan', $data);
    }
}
