<?php

namespace App\Http\Controllers;

use App\modelDbarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

class controllerMasterBarang extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('supplier')->select('*')->get();
        $dataLempar = [];
        $dataLempar['temp'] = $data;
        return view('frontEnd/frontEndMasterBarang', $dataLempar);
    }
    public function inputSupplier(Request $request){
        $data = $request->post('id_supplier');
        $input = $request->all();


        //$data = $request->post("id_supplier");
        $dataBarang = DB::table('barang_supplier')
                    ->join('barang', 'barang.id_barang', '=', 'barang_supplier.id_barang')
                    ->select('*')
                    ->where('id_supplier', $data)
                    ->get();
                    //var_dump($dataBarang);
        $view = view('frontEnd/ajaxMasterBarang', ['barangSupplier'=>$dataBarang, 'id_supplier' => $data])->render();
        return response()->json(['success' => $dataBarang, 'html' => $view]);

    }
    public function tambahBarang(Request $request){
        $data = $request->post();
        $itemCart = session('itemCart');
        $id_barang = $data["id_barang"];
        $jenis = $data["jenis"];

        $itemCart[$id_barang][$jenis]["jumlah"] += 1;
        session(["itemCart" => $itemCart]);

        $view = view('frontEnd/ajaxCart')->render();
        return response()->json(['html' => $view]);

    }
    public function checkout(Request $request){
        $item = session('itemCart');
        //dump($item);
        foreach ($item as $value) {
            foreach ($value as $value2) {
                if ($value2 != null){
                    $id_barang = $value2['id_barang'];
                    $jenis = $value2['jenis'];
                    $end = date('Y-m-d', strtotime('+5 years'));
                    $model = new modelDbarang;
                    $data = $model::where('id_barang', $id_barang)->where('jenis', $jenis)->where('expired', $end)->first();
                    //dump($data);
                    if ($data != null){

                        $jmlObat = (int)$data->stock + (int)$value2['jumlah'];

                        $barangSudahAda = $model::find($data->id_dbarang);
                        $barangSudahAda->stock = $jmlObat;
                        $barangSudahAda->save();

                    } else {
                        $model->id_barang = $id_barang;
                        $model->stock = $value2['jumlah'];
                        $model->expired = date('Y-m-d', strtotime('+5 years'));
                        $model->jenis = $value2['jenis'];
                        $model->save();
                    }

                }
            }
        }
        $request->session()->forget('itemCart');
        return redirect('masterBarang');
    }
    public function kecheckout(Request $request){
        return view('frontEnd/checkOutMasterBarang');
    }
    public function ajaxTabel(Request $request){
        $view = view('frontEnd/ajaxCart')->render();
        return response()->json(['html' => $view]);
    }
    public function kurangBarang(Request $request){
        $data = $request->post();
        $itemCart = session('itemCart');
        $id_barang = $data["id_barang"];
        $jenis = $data["jenis"];

        $jumlahBarang = (int)$itemCart[$id_barang][$jenis]["jumlah"];

        if ($jumlahBarang > 1){
            $itemCart[$id_barang][$jenis]["jumlah"] -= 1;
        } else {
            unset($itemCart[$id_barang][$jenis]);
        }
        session(["itemCart" => $itemCart]);
        $view = view('frontEnd/ajaxCart')->render();
        return response()->json(['html' => $view]);
    }
    public function addToCart(Request $request)
    {
        $data = $request->post();
        //dd($data);
        $id_barang = $data["id_barang"];
        $nama = $data["nama"];
        $jumlah = $data['jumlah'];
        $harga = $data["harga"];
        $jenis = $data['jenis'];
        if ($jenis == "box"){
            $harga = $harga * 36;
        } else if ($jenis == "strip"){
            $harga = $harga * 6;
        } else {
            $harga = $harga;
        }
        $cart = [
            "id_barang" => $id_barang,
            "nama" => $nama,
            "jumlah" => $jumlah,
            "jenis" => $jenis,
            "harga" => $harga
        ];

        $itemCart = session('itemCart');
        if ($cart["jumlah"] > 0){
            if ($itemCart != null){
                if (isset($itemCart[$id_barang][$jenis])){
                    $jmlTotal = (int)$itemCart[$id_barang][$jenis]["jumlah"] + $cart["jumlah"];
                    $itemCart[$id_barang][$jenis]["jumlah"] = $jmlTotal;
                    session(["itemCart" => $itemCart]);
                    echo "berhasil menambahkan ".$nama;
                } else {
                    $itemCart[$id_barang][$jenis] = $cart;
                    session(["itemCart" => $itemCart]);
                    echo "berhasil menambahkan ".$nama;
                }
            } else {
                $itemCart[$id_barang][$jenis] = $cart;
                session(["itemCart" => $itemCart]);
                echo "berhasil menambahkan ".$nama;
            }
        } else {
            echo "jumlah barang yang di inputkan tidak sesuai";
        }
    }
}
