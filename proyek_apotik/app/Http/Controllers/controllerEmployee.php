<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\d_barang;

class controllerEmployee extends Controller
{
    public function index(Request $request){
        $waktu = date('Y-m-d');
        //$tempbarang = DB::table('barang')->join('d_barang','barang.id_barang','=','d_barang.id_barang')->where('d_barang.status','=',1)->select("barang.nama_barang", "barang.fungsi", "barang.harga", "d_barang.stock", "d_barang.jenis", "d_barang.expired")->get();

        $tempbarang = d_barang::where('status','=',1)->get();

        $data=[
            'allbarang'=>$tempbarang
        ];
        return view("employee",$data);
    }

    public function open_employee(Request $request){
        if($request->input("logout")){
            return view("login");
        }
        else if($request->input('laporan_barang_expired')){
            //$temp = DB::table('barang')->join('d_barang','barang.id_barang','=','d_barang.id_barang')->where('d_barang.status','=',0)->select("barang.nama_barang", "barang.fungsi", "barang.harga", "d_barang.stock", "d_barang.jenis", "d_barang.expired")->get();
            $temp = d_barang::where('status','=',0)->get();
            $data = [
                'allbarang' => $temp
            ];
            return view('laporan_employee',$data);
        }
        else if($request->input('update_expired')){
            $waktu = date('Y-m-d');
            DB::table('barang')->join('d_barang','barang.id_barang','=','d_barang.id_barang')->whereDate('d_barang.expired','<=',$waktu)->update(['d_barang.status' => 0]);
            return redirect('employee/page_employee');
        }
    }

    public function open_laporan_employee(Request $request){
        if($request->input("logout")){
            return view("login");
        }
        else if($request->input('laporan_barang')){
            return redirect('employee/page_employee');
        }
    }
}

?>
