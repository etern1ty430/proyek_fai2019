<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\Nota_Pembelian_Customer_Docmec;
use Illuminate\Support\Facades\Mail;

class controllerKasir extends Controller
{
    public function index(Request $request)
    {
        $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
        $data=[
            'allmember'=>$tempmember
        ];
        return view('register_member_kasir',$data);
    }
    public function open_register_member(Request $request){
        if($request->input('logout') == true){
            $request->session()->pull('user', '');
            $request->session()->pull('acc_type', '');
            $request->session()->pull('tempdatabase', '');
            $request->session()->pull('cart', '');
            $request->session()->pull('total', '');
            $request->session()->pull('diskon', '');
            $request->session()->pull('poin_user', '');
            $request->session()->pull('poin_yang_dipakai', '');
            return redirect('proyek/login');
        }
        if($request->input('register')==true){
            $namalengkap = $request->input('namalengkap');
            $tgllahir = $request->input('tgllahir');
            $notelp = $request->input('notelp');
            $gender = $request->input('gender');
            $email = $request->input('email');
            $tempmember = [];
            $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
            $max=-1;
            $username="";
            foreach ($tempmember as $r) {
                if($max < (int)substr($r->username,6,1)){
                    $max=(int)substr($r->username,6,1);
                }
            }
            if($max==-1){
                $username="member0";
            }
            else{
                $max=$max+1;
                $username="member".$max;
            }
            $arr = [
                "username" => $username,
                "email" => $email,
                "password" => '',
                "nama" => $namalengkap,
                "tgllhr" => $tgllahir,
                "notelp" => $notelp,
                "gender" => $gender,
                "status" => 'M',
                "poin" => 0,
            ];
            DB::table('account')->insert($arr);
            $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
            $data=[
                'allmember'=>$tempmember
            ];
            return view('register_member_kasir',$data);
        }
        else if($request->input('find')==true){
            $filter=$request->input('filter');
            $tempmember = DB::table('account')->select("*")->where('nama', 'like', "%$filter%")->where("status", "=", 'M')->get();
            $data=[
                'allmember'=>$tempmember
            ];
            return view('register_member_kasir',$data);
        }
        else{
            $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
            $tempbarang = DB::table('barang')->select("*")->get();
            $tempdbarang = DB::table('d_barang')->select("*")->where("status","=","1")->get();
            $ketemu=false;
            $member="";
            foreach ($tempmember as $r) {
                if($request->input($r->username)==true){
                    $ketemu=true;
                    $member=$r->username;
                }
            }
            if($ketemu==true){
                $data=[
                    'allmember'=>$tempmember,
                    'allbarang'=>$tempbarang,
                    'alldbarang'=>$tempdbarang
                ];
                $tempdatabase=[];
                $request->session()->put('member', $member);
                foreach ($tempbarang as $r) {
                    $tempdata1 = DB::table('d_barang')->where('id_barang',$r->id_barang)
                    ->where('jenis','biji')->where("status","=","1")->groupBy('id_barang','jenis')->sum('stock');
                    if($tempdata1!=0){
                        array_push($tempdatabase,array(
                            'id_barang'=>$r->id_barang,
                            'nama_barang'=>$r->nama_barang,
                            'fungsi'=>$r->fungsi,
                            'harga'=>$r->harga,
                            'stock'=>$tempdata1,
                            'jenis'=>'biji',
                            'img_resource'=>$r->img_resource
                        ));
                    }
                    $tempdata2 = DB::table('d_barang')->where('id_barang',$r->id_barang)
                    ->where('jenis','strip')->where("status","=","1")->groupBy('id_barang','jenis')->sum('stock');
                    if($tempdata2!=0){
                        array_push($tempdatabase,array(
                            'id_barang'=>$r->id_barang,
                            'nama_barang'=>$r->nama_barang,
                            'fungsi'=>$r->fungsi,
                            'harga'=>(int)$r->harga*6,
                            'stock'=>$tempdata2,
                            'jenis'=>'strip',
                            'img_resource'=>$r->img_resource
                        ));
                    }
                    $tempdata3 = DB::table('d_barang')->where('id_barang',$r->id_barang)
                    ->where('jenis','box')->where("status","=","1")->groupBy('id_barang','jenis')->sum('stock');
                    if($tempdata3!=0){
                        array_push($tempdatabase,array(
                            'id_barang'=>$r->id_barang,
                            'nama_barang'=>$r->nama_barang,
                            'fungsi'=>$r->fungsi,
                            'harga'=>(int)$r->harga*36,
                            'stock'=>$tempdata3,
                            'jenis'=>'box',
                            'img_resource'=>$r->img_resource
                        ));
                    }
                }
                $request->session()->put('tempdatabase', $tempdatabase);
                return view('order_kasir',$data);
            }
            else{
                $data=[
                    'allmember'=>$tempmember
                ];
                return view('register_member_kasir',$data);
            }
        }
    }
    public function open_order_kasir (Request $request){
        if($request->input('cancel') == true){
            $request->session()->pull('tempdatabase', '');
            $request->session()->pull('cart', '');
            $request->session()->pull('total', '');
            $request->session()->pull('diskon', '');
            $request->session()->pull('poin_user', '');
            $request->session()->pull('poin_yang_dipakai', '');
            $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
            $data=[
                'allmember'=>$tempmember
            ];
            return view('register_member_kasir',$data);
        }
        if($request->input('logout') == true){
            $request->session()->pull('user', '');
            $request->session()->pull('acc_type', '');
            $request->session()->pull('tempdatabase', '');
            $request->session()->pull('cart', '');
            $request->session()->pull('total', '');
            $request->session()->pull('diskon', '');
            $request->session()->pull('poin_user', '');
            $request->session()->pull('poin_yang_dipakai', '');
            return redirect('proyek/login');
        }
        if($request->input('checkout')==true){
            $cart=$request->session()->get('cart');
            if($cart!=null){
                $allmember = DB::table('account')->select("*")->get();
                $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
                $tempbarang = DB::table('barang')->select("*")->get();
                $tempdbarang = DB::table('d_barang')->select("*")->where("status","=","1")->get();
                $data=[
                    'allmembers'=>$allmember,
                    'allmember'=>$tempmember,
                    'allbarang'=>$tempbarang,
                    'alldbarang'=>$tempdbarang
                ];
                $count=DB::table('h_jual')->count();
                $count++;
                $notajual="";
                if($count<10){
                    $notajual="NJ00".$count;
                }
                else if($count<100){
                    $notajual="NJ0".$count;
                }
                else if($count<1000){
                    $notajual="NJ".$count;
                }

                $poin_yang_dipakai=$request->input('text_isi');
                $cart=$request->session()->get('cart');
                $total_sebelumnya=$request->session()->get('total');
                $total=$request->input('totalharga_sekarang');
                $diskon=$total_sebelumnya-$total;
                //isi session
                $request->session()->put("diskon",$diskon);
                $request->session()->put("poin_yang_dipakai",$poin_yang_dipakai);
                $request->session()->put("total",$total);
                $date = date('Y-m-d');
                $arr = [
                    'nota_jual'=>$notajual,
                    'username_cust'=>$request->session()->get('member'),
                    'username_peg'=>$request->session()->get('user'),
                    'tanggal'=>$date,
                    'total'=>$total
                ];
                DB::table('h_jual')->insert($arr);
                foreach ($cart as $r) {
                    $harga_item=0;
                    foreach ($tempbarang as $r2) {
                        if($r2->id_barang==$r['id_barang']){
                            $harga_item=$r2->harga;
                        }
                    }
                    $arr2 = [
                        'id_djual'=>null,
                        'nota_jual'=>$notajual,
                        'id_barang'=>$r['id_barang'],
                        'qty'=>$r['stock'],
                        'jenis'=>$r['jenis'],
                        'harga'=>$harga_item
                    ];
                    DB::table('d_jual')->insert($arr2);
                }
                return view('invoice_kasir',$data);
            }
            else{
                $allmember = DB::table('account')->select("*")->get();
                $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
                $tempbarang = DB::table('barang')->select("*")->get();
                $tempdbarang = DB::table('d_barang')->select("*")->where("status","=","1")->get();
                $data=[
                    'allmembers'=>$allmember,
                    'allmember'=>$tempmember,
                    'allbarang'=>$tempbarang,
                    'alldbarang'=>$tempdbarang
                ];
                echo "<script>alert('Cart Kosong!')</script>";
                return view('order_kasir',$data);
            }
        }
        else{
            $allmember = DB::table('account')->select("*")->get();
            $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
            $tempbarang = DB::table('barang')->select("*")->get();
            $tempdbarang = DB::table('d_barang')->select("*")->where("status","=","1")->get();
            $data=[
                'allmembers'=>$allmember,
                'allmember'=>$tempmember,
                'allbarang'=>$tempbarang,
                'alldbarang'=>$tempdbarang
            ];
            $tempdatabase=$request->session()->get('tempdatabase');
            $index=0;
            $cart=[];
            if($request->session()->get('cart')!=null){
                $cart=$request->session()->get('cart');
            }
            foreach ($tempdatabase as $r) {
                if($request->input('order'.$index)){
                    if($cart==null){
                        array_push($cart,array(
                            'id_barang'=>$r['id_barang'],
                            'nama_barang'=>$r['nama_barang'],
                            'fungsi'=>$r['fungsi'],
                            'harga'=>$r['harga'],
                            'stock'=>'1',
                            'jenis'=>$r['jenis']
                        ));
                        if($tempdatabase[$index]['stock']>0){
                            $tempdatabase[$index]['stock']=$tempdatabase[$index]['stock']-1;
                        }
                    }
                    else{
                        if($tempdatabase[$index]['stock']>0){
                            $tempdatabase[$index]['stock']=$tempdatabase[$index]['stock']-1;
                            $cekberhasil=false;
                            for ($i=0; $i < count($cart); $i++) {
                                if($cart[$i]['id_barang']==$tempdatabase[$index]['id_barang'] && $cart[$i]['jenis']==$tempdatabase[$index]['jenis']){
                                    $cart[$i]['stock']=$cart[$i]['stock']+1;
                                    $cekberhasil=true;
                                }
                            }
                            if($cekberhasil==false){
                                array_push($cart,array(
                                    'id_barang'=>$r['id_barang'],
                                    'nama_barang'=>$r['nama_barang'],
                                    'fungsi'=>$r['fungsi'],
                                    'harga'=>$r['harga'],
                                    'stock'=>'1',
                                    'jenis'=>$r['jenis']
                                ));
                            }
                        }
                    }
                    $request->session()->put('cart', $cart);
                    $request->session()->put('tempdatabase', $tempdatabase);
                }
                $index++;
            }
            $index=0;
            foreach ($cart as $r) {
                if($request->input('kurangi'.$index)){
                    if($cart[$index]['stock']-1>0){
                        $cart[$index]['stock']=$cart[$index]['stock']-1;
                        for ($i=0; $i < count($tempdatabase) ; $i++) {
                            if($cart[$index]['id_barang']==$tempdatabase[$i]['id_barang'] && $cart[$index]['jenis']==$tempdatabase[$i]['jenis']){
                                $tempdatabase[$i]['stock']=$tempdatabase[$i]['stock']+1;
                            }
                        }
                    }
                    else{
                        for ($i=0; $i < count($tempdatabase) ; $i++) {
                            if($cart[$index]['id_barang']==$tempdatabase[$i]['id_barang'] && $cart[$index]['jenis']==$tempdatabase[$i]['jenis']){
                                $tempdatabase[$i]['stock']=$tempdatabase[$i]['stock']+1;
                            }
                        }
                        unset($cart[$index]);
                        $cart=array_values($cart);
                    }
                    $request->session()->put('cart', $cart);
                    $request->session()->put('tempdatabase', $tempdatabase);
                }
                $index++;
            }
            return view('order_kasir',$data);
        }
    }

    public function open_invoice(Request $request){
            $cart=$request->session()->get('cart');
            //for sebanyak barang yang ada di cart
            for ($i=0; $i <count($cart) ; $i++) {
                $banyakbarang=$cart[$i]['stock'];
                //for sebanyak banyak barang yang dibeli
                for ($j=0; $j < $banyakbarang; $j++) {
                    $tempdbarang = DB::table('d_barang')->select("*")->where("status","=","1")->where("id_barang", "=",$cart[$i]['id_barang'] )->where("jenis","=",$cart[$i]['jenis'])->get();
                    $min=9999999999;
                    $id_pilihan=-1;
                    $stok_sekarang=-1;
                    //echo $cart[$i]['id_barang'];
                    foreach ($tempdbarang as $r) {
                        $splitexpired=explode("-",$r->expired);
                        $total=($splitexpired[0]*365)+($splitexpired[1]*30)+$splitexpired[2];
                        if($min>$total){
                            $min=$total;
                            $id_pilihan=$r->id_dbarang;
                            $stok_sekarang=$r->stock;
                        }
                    }
                    //echo "<script>alert($min)</script>";
                    if($id_pilihan!=-1){
                        if($stok_sekarang=="1"){
                            $arr = [
                                "stock"=>0,
                                "status"=>2
                            ];
                            DB::table('d_barang')->where("id_dbarang", "=", $id_pilihan)->where("status","=","1")->update($arr);
                        }
                        else if($stok_sekarang>1){
                            $arr = ["stock"=>$stok_sekarang-1];
                            DB::table('d_barang')->where("id_dbarang", "=", $id_pilihan)->where("status","=","1")->update($arr);
                        }
                    }
                }
            }
            $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
            $data=[
                'allmember'=>$tempmember
            ];
            $user_yang_dipilih=session()->get('member');
            $data_user_yang_dipilih=DB::table('account')->where("username", "=", $user_yang_dipilih)->first();
            $poin_user_yang_dipilih=$data_user_yang_dipilih->poin;
            $poin_yang_didapat=intval((session()->get('total'))/10000);
            $poin_yang_dipakai=session()->get('poin_yang_dipakai');
            //untuk mengurangi poin didatabase
            $total_poin_sekarang=$poin_user_yang_dipilih+$poin_yang_didapat-$poin_yang_dipakai;
            $arr = ["poin"=>$total_poin_sekarang];
            DB::table('account')->where("username", "=", $user_yang_dipilih)->update($arr);
            echo "<script>alert($data_user_yang_dipilih->email)</script>";
            Mail::to($data_user_yang_dipilih->email)->send(new Nota_Pembelian_Customer_Docmec());

            //bakar session
            $request->session()->pull('tempdatabase', '');
            $request->session()->pull('cart', '');
            $request->session()->pull('total', '');
            $request->session()->pull('diskon', '');
            $request->session()->pull('poin_user', '');
            $request->session()->pull('poin_yang_dipakai', '');
            return view('register_member_kasir',$data);
    }
}
