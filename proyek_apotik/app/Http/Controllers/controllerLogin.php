<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;


class controllerLogin extends Controller
{
    public function index(Request $request)
    {
        return view('login');
    }

    public function openlogin(Request $request){
        if($request->input('login') == true){
            $username = $request->input('username');
            $password = $request->input('password');

            if($username == "master" && $password == "master") {
                return redirect("masterBarang/");
            }
            else if($username == "supplier" && $password == "supplier"){
                return redirect("supplier/pagesupplier");
            }
            else {
                $cariAcc = DB::table('account')
                ->where("username","=",$username)->where("password","=",$password)
                ->count();
                if($cariAcc == 1){
                    $getData = DB::table('account')->select("*")
                    ->where("username","=",$username)->where("password","=",$password)
                    ->get();

                    $request->session()->put('user', $getData[0]->username);
                    $request->session()->put('acc_type', $getData[0]->status);

                    if($getData[0]->status == "C") return redirect("kasir/registermember");
                    else if($getData[0]->status == "E") return redirect('employee/page_employee');
                }
                else {
                    $request->session()->flash('error', 'Username or Password Wrong!!!');
                    return view('login', $request);
                }
            }
        }
        else if($request->input('forgetpass') == true){
            return view('forgetpassword');
        }
    }

    public function openforgotpassword(Request $request)
    {
        $username = $request->input('username');
        $oldpassword = $request->input('oldpassword');
        $password = $request->input('password');
        $confpass = $request->input('confpass');

        $accActive = DB::table('account')
        ->where("username","=",$username)
        ->count();

        if($accActive == 1){
            $pwold = DB::table('account')->select("password")->where("username","=",$username)->get();
            if($pwold[0]->password == $oldpassword){
                if($password == $confpass){
                    $arr = [
                        "password" => $password
                    ];
                    DB::table('account')
                    ->where("username", "=", $username)
                    ->update($arr);
                }
                else {
                    //kasi error kalo confirm pass dan new passnya gk sama
                    $request->session()->flash('error', 'Password And Confirm Must Same!!!');
                }
            }
            else {
                //kasi error kalo password old nya salah
                $request->session()->flash('error', 'Old Password Wrong!!!');
            }
        }
        else {
            //kalo g msk kasi error username tdk trdaftar
            $request->session()->flash('error', 'Username Not Registered!!!');
        }

        return view('forgetpassword',$request);
    }

    public function openregister(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $confpass = $request->input('password_confirmation');
        $nama = $request->input('nama');
        $tgllhr = $request->input('tgllhr');
        $notelp = $request->input('notelp');
        $rbGender = $request->input('rbGender');
        $rbStatus = $request->input('rbStatus');

        $validasi = $this->validate($request,
            [
                "username"=>"required",
                "password"=>"required|confirmed",
                "password_confirmation"=>"required",
                "nama"=>"required",
                "tgllhr"=>"required",
                "notelp"=>"required"
            ],
            [
                'username.required'=>"field username must be filled",
                'password.required'=>"field password_confirm must be filled",
                'password.confirmed'=>"password and confirm must same",
                'password_confirmation.required'=>"field password must be filled",
                'nama.required'=>"field nama must be filled",
                'tgllhr.required'=>"field tgllhr must be filled",
                'notelp.required'=>"field notelp must be filled"
            ]
        );

        if($password == $confpass){
            $arr = [
                "username" => $username,
                "password" => $password,
                "nama" => $nama,
                "tgllhr" => $tgllhr,
                "notelp" => $notelp,
                "gender" => $rbGender,
                "status" => $rbStatus,
                "poin" => 0,
            ];
            DB::table('account')->insert($arr);
        }
        return view('register',$request);
    }
}
