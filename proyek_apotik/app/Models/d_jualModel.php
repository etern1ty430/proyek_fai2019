<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class d_jualModel extends Model
{
    protected $table = "d_jual";
    protected $primaryKey = "id_djual";
    public $timestamps = false;
    protected $connection = "mysql";
    protected $fillable = ["id_djual", "nota_jual", "id_barang", "qty", "jenis", "harga"];

    public function nama_barang()
    {
        return $this->belongsTo('App\Models\BarangModel', "id_barang", "id_barang");
    }
}
