<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = "barang";
    protected $primaryKey = "id_barang";
    public $timestamps = false;
    protected $connection = "mysql";

    public function barangs()
    {
        return $this->hasMany("App\Models\AccountModel", "id_barang", "id_barang");
    }
}
