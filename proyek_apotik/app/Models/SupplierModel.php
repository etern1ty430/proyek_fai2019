<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierModel extends Model
{
    protected $table = "supplier";
    protected $primaryKey = "id_supplier";
    public $timestamps = false;
    protected $connection = "mysql";
    public function suppliers()
    {
        return $this->hasMany("App\Models\h_beliModel", "id_supplier", "id_supplier");
    }
}
