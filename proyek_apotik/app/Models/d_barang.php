<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class d_barang extends Model
{
    protected $table = "d_barang";
    protected $primaryKey = "id_dbarang";
    public $timestamps = false;
    protected $connection = "mysql";
    protected $fillable = ["id_dbarang", "id_barang", "stock", "expired", "jenis", "status"];

    public function connect_barang()
    {
        return $this->belongsTo('App\Models\BarangModel', "id_barang", "id_barang");
    }
}
