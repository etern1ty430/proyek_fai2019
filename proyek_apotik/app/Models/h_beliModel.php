<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class h_beliModel extends Model
{
    protected $table = "h_beli";
    protected $primaryKey = "nota_beli";
    public $timestamps = false;
    protected $connection = "mysql";
    protected $fillable = ["nota_beli", "id_supplier", "tanggal", "total"];

    public function nama_supplier()
    {
        return $this->belongsTo('App\Models\SupplierModel', "id_supplier", "id_supplier");
    }
}
