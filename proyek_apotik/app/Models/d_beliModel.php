<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class d_beliModel extends Model
{
    protected $table = "d_beli";
    protected $primaryKey = "id_dbeli";
    public $timestamps = false;
    protected $connection = "mysql";
    protected $fillable = ["id_dbeli", "nota_beli", "id_barang", "qty", "jenis", "harga"];

    public function nama_barang()
    {
        return $this->belongsTo('App\Models\BarangModel', "id_barang", "id_barang");
    }
}
