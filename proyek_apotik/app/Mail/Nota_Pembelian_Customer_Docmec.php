<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\DB;
class Nota_Pembelian_Customer_Docmec extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $allmember = DB::table('account')->select("*")->get();
        $tempmember = DB::table('account')->select("*")->where("status", "=", 'M')->get();
        $tempbarang = DB::table('barang')->select("*")->get();
        $tempdbarang = DB::table('d_barang')->select("*")->where("status","=","1")->get();
        $data=[
            'allmembers'=>$allmember,
            'allmember'=>$tempmember,
            'allbarang'=>$tempbarang,
            'alldbarang'=>$tempdbarang
        ];
        return $this->from('revelution430@gmail.com')
                   ->view('isi_email_nota_kasir',$data)
                   ->with(
                    [
                        'nama' => 'Edwin Sidharta',
                        'website' => 'www.DocMec.com',
                    ]);
    }
}
