<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('login');
});

Route::group(['prefix' => 'proyek'], function () {
    Route::get('login', 'controllerLogin@index');
    Route::post('processlogin', 'controllerLogin@openlogin');

    Route::get('pageforgotpassword', function () { return view('forgetpassword'); });
    Route::post('processforgot', 'controllerLogin@openforgotpassword');

    Route::get('pageregister', function () { return view('register'); });
    Route::post('processregister', 'controllerLogin@openregister');

    Route::get('pageLaporan', function (){ return view('laporan'); });
    Route::post('processlaporan', 'controllerLaporan@openLaporan');
});

Route::group(['prefix' => 'supplier'], function () {
    Route::get('pagesupplier', function () { return view('supplieradd'); });
    Route::post('processsupplier', 'controllerSupplier@openaddsupplier');

    Route::get('pagesupplierbarang', function () { return view('supplieradd_barang'); });
    Route::post('processsupplierbarang', 'controllerSupplier@openaddsupplier_barang');
});

Route::group(['prefix' => 'kasir'], function () {
    Route::group(['middleware' => 'cekjabatan:C'], function () {
        Route::get('registermember', 'controllerKasir@index');

        Route::get('pageregister', function (){ return view('register_member_kasir'); });
        Route::post('processregister', 'controllerKasir@open_register_member');

        Route::get('pageorder', function (){ return view('order_kasir'); });
        Route::post('processorder', 'controllerKasir@open_order_kasir');

        Route::get('pageinvoice', function (){ return view('invoice_kasir'); });
        Route::post('processinvoice', 'controllerKasir@open_invoice');

        Route::get('kirimemail','controllerKasir@kirim_email');
    });
});

Route::group(['prefix' => 'masterBarang'], function () {
    Route::get('', 'controllerMasterBarang@index');
    Route::get('checkOut', 'controllerMasterBarang@checkout');
    Route::get('keCheckout', 'controllerMasterBarang@kecheckout');
    Route::get('cart', function (){
        return view("frontEnd/cart");
    });
    Route::post('inputSupplier', 'controllerMasterBarang@inputSupplier');
    Route::post('inputBarang', 'controllerMasterBarang@inputBarang');
    Route::post('addToCart', 'controllerMasterBarang@addToCart');
    Route::post('tambahBarang', 'controllerMasterBarang@tambahBarang');
    Route::post('kurangBarang', 'controllerMasterBarang@kurangBarang');
    Route::post('ajaxTabel', 'controllerMasterBarang@ajaxTabel');
});

Route::group(['prefix' => 'employee'], function () {
    Route::group(['middleware' => 'cekjabatan:E'], function () {
        Route::get('page_employee', 'controllerEmployee@index');
		Route::post('process_employee', 'controllerEmployee@open_employee');

		Route::get('page_laporan_employee', function(){return view('laporan_employee'); });
		Route::post('process_laporan_employee', 'controllerEmployee@open_laporan_employee');
    });
});
