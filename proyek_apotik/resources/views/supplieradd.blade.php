<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ URL::to('/') }}/images/loginBackground.jpg");
            background-size: cover;
            color: white;
        }
    </style>
</head>
<body> <br><br>
    <div class='row' style="width:100%;">
        <div class="col-md-4"></div>
        <div class="col-md-4" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%; padding-left:3%;'>
            <br>
            <form action="{{url('supplier/processsupplier')}}" method="post">
            @csrf
                <center><h1> Add New Supplier</h1></center><br><br>
                <div class="form-group">
                    Name : <input type="text" name="name" id="name" style="width:50%;margin-left:19%;"><br><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    Address : <input type="text" name="address" id="address" style="width:50%;margin-left:16%;"><br><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    Phone Number : <input type="text" name="phone" id="phone" style="width:50%;margin-left:5%;"><br><br>
                </div>
                <center>
                    <input type="submit" value="Submit" name="submit" id="submit" class="btn btn-outline-primary btn-md">
                    <input type="submit" value="Input Stock" name="stock" id="stock" class="btn btn-outline-primary btn-md">
                <br>
                    <div class="alert" >
                        @if (Session::has('error'))
                            {{Session::get('error')}}
                        @endif
                    </div>
                </center>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</body>
</html>



