<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
    body{
        background-image: url("{{ URL::to('/') }}/images/registerBackground.jpg");
        background-size: cover;
        color:white;
    }
    </style>
</head>
<body>
    <div class='row' style="width:100%;">
        <div class="col-md-4"></div>

        <div class="col-md-4" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
        <form action="{{url('proyek/processregister')}}" method="post">
            @csrf
            <br>
            <center>
            <div>
                <h1 style='color:white;'>REGISTER</h1>
                <br>
                <div class="form-group">
                    <label for="username" style='color:white;'>Username</label><br>
                    <center><input type="text" name="username" id="" value="{{old("username")}}" style="width:50%;"></center><br>
                    @error("username")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="password" style='color:white;'>Password</label><br>
                    <center><input type="password" name="password" id="" value="{{old("password")}}" style="width:50%;"></center><br>
                    @error("password_confirmation")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="password" style='color:white;'>Confirm Password</label><br>
                    <center><input type="password" name="password_confirmation" value="{{old("password_confirmation")}}" style="width:50%;"></center><br>
                    @error("password")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="nama" style='color:white;'>Nama</label><br>
                    <center><input type="text" name="nama" id="nama" value="{{old("nama")}}" style="width:50%;"></center><br>
                    @error("nama")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="tgllhr" style='color:white;'>Birth Date</label><br>
                    <center><input type="date" name="tgllhr" id="tgllhr" value="{{old("tgllhr")}}" style="width:50%;"></center><br>
                    @error("tgllhr")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="notelp" style='color:white;'>Phone Number</label><br>
                    <center><input type="text" name="notelp" id="notelp" value="{{old("notelp")}}" style="width:50%;"></center><br>
                    @error("notelp")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="rbGender" style='color:white;'>Gender</label><br>
                    <center>
                        <input type="radio" name="rbGender" id="rbGender" value="L" style="width:5%; margin-left:-3%;" checked> <img src="/images/male.png" style="width:5%; height:5%;">
                        <input type="radio" name="rbGender" id="rbGender" value="P" style="width:5%; margin-left:10%;"> <img src="/images/female.png" style="width:6%; height:6%;">
                    </center><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="rbStatus" style='color:white;'>Status</label><br>
                    <center>
                        <input type="radio" name="rbStatus" id="rbStatus" value="E" style="width:5%; margin-left:-3%;" checked  > Employee
                        <input type="radio" name="rbStatus" id="rbStatus" value="C" style="width:5%; margin-left:10%;"> Cashier
                    </center><br>
                </div>
                <button class="btn btn-outline-primary btn-md"> Register</button>
        </form>
        <form action="{{url('')}}" method="get">
            <br><a href="{{url('')}}"><button class="btn btn-outline-primary btn-md"> Back to Login</button></a>
        </form>
            </center> <br>
            </div>
        </div>
        <div class="col-md-4"></div><br><br>
    </div>
</body>
</html>

