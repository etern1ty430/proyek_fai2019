<!DOCTYPE html>
<head>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
            body{
                background-image: url("{{ URL::to('/') }}/images/laporanBackground.jpg");
                background-size: cover;
                color:white;
            }
            hr {
                -moz-border-bottom-colors: none;
                -moz-border-image: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                border-color: #EEEEEE -moz-use-text-color #FFFFFF;
                border-style: solid none;
                border-width: 1px 0;
                margin: 18px 0;
            }
        </style>
</head>
<body>
<div class='row' style="width:100%;">
        <div class="col-md-3"></div>
        <div class="col-md-6" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
            <form action="{{url('kasir/processinvoice')}}" method="post">
                @csrf
                {{-- ambil data --}}
                @php
                    $cart=Session::get('cart');
                    $total=0;
                    $subtotal=0;
                    $total=Session::get('total');
                    $diskon=Session::get('diskon');
                    $total_poin=Session::get('poin_yang_dipakai');
                @endphp
                <hr>
                <br>
                {{-- bagian title nya --}}
                <div class='row' style="width:100%;">
                    <div class="col-md-2" style="margin-top:1%"><img src="{{url('/images/iconDocmec.jpg')}}" style="margin:3% 0% 0% 50%;"></div>
                    <div class="col-md-2" ></div>
                    <div class="col-md-8" >
                        <h2>Invoice Pembelian Obat Apotik Docmec</h2>
                        <h5>Ngagel Jaya Tengah 73 - 77, Surabaya, Jawa Timur</h5>
                        <h5>Contact : 14045</h5>
                    </div>
                </div>
                <br><hr>
                <div class='row' style="width:100%;">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        {{-- <h2>Kasir :
                            @foreach ($allmembers as $r)
                                @if ($r->username==Session::get('user'))
                                    {{$r->nama}}
                                @endif
                            @endforeach
                        </h2> --}}
                        <h4>Nama Customer :
                            @foreach ($allmember as $r)
                                @if ($r->username==Session::get('member'))
                                    <span style="text-transform:capitalize">{{($r->nama)}}</span>
                                @endif
                            @endforeach
                        </h4>
                        <br>
                        <h4>Daftar Obat Yang Dibeli : </h4>
                    </div>
                    <div class="col-md-8"></div>
                </div>

                <div class='row' style="width:100%;">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <center><h6>
                            <table border="1 solid white" cellpadding=15>
                                <tr>
                                    <td>Nama Obat : </td>
                                    <td>Harga/Pcs</td>
                                    <td>Jumlah</td>
                                    <td>Subtotal</td>
                                </tr>
                                @foreach ($cart as $r)
                                    @php
                                        $subtotal=(int)($r['harga']*$r['stock']);
                                    @endphp
                                    <tr>
                                        <td>{{$r['nama_barang']}}</td>
                                        <td>{{'Rp '.number_format($r['harga'], 2, ",",".").' / '.'1 '.$r['jenis']}}</td>
                                        <td>{{$r['stock']." ".$r['jenis']}}</td>
                                        <td>{{'Rp '.number_format($subtotal, 2, ",",".")}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </h6></center>
                            <h5 align="right" style="margin-right:8%;">{{'Total : Rp '.number_format(($total+$diskon), 2, ",",".")}}</h5>
                            <h5 align="right" style="margin-right:8%;">{{'Diskon : - Rp '.number_format($diskon, 2, ",",".")}}</h5>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
                        <hr>
                        <h4 align="center">{{'Total Yang Harus Dibayar Adalah Rp '.number_format($total, 2, ",",".")}}</h4>
                        <h4 align="center">{{'(Anda Mendapatkan : '.(intval($total/10000)).' Poin)'}}</h4><br>
                <div align="center">
                    <input class="btn btn-success" style type="submit" value="Lanjutkan" name="next" id="next"><br><hr>
                </div>
            </form>

        </div>
        <div class="col-md-3"></div>
</div>
</body>
</html>
