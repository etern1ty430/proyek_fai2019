<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ URL::to('/') }}/images/loginBackground.jpg");
            background-size: cover;
            color: white;
        }
    </style>
</head>
<body> <br><br>
    <div class='row' style="width:100%;">
        <div class="col-md-4"></div>
        <div class="col-md-4" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%; padding-left:3%;'>
            <br>
            <form action="{{url('supplier/processsupplierbarang')}}" method="post" enctype="multipart/form-data">
            @csrf
                <center><h1> Add Barang</h1></center><br><br>
                <div class="form-group">
                    Product Name : <input type="text" name="name" id="name" style="width:50%;margin-left:3.5%;"><br><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    ID Supplier :
                    <select name = "idsupplier" id = "idsupplier" style="width:50%;margin-left:8.5%;">
                        @foreach ($supplier as $item)
                            <option value={{$item->id_supplier}}> {!!$item->id_supplier!!} </option>
                        @endforeach
                    </select><br><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    Function : <input type="text" name="fungsi" id="fungsi" style="width:50%;margin-left:12.5%;"><br><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    Supplier Price : <input type="text" name="sPrice" id="sPrice" style="width:50%;margin-left:4.5%;"><br><br>
                </div>

                <div class="form-group" style="margin-top:-5%;">
                    Product Price : <input type="text" name="pPrice" id="pPrice" style="width:50%;margin-left:5%;"><br><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    Upload Gambar : <input type="file" name="uploadkan" id="" style="width:50%;margin-left:5%;"><br><br>
                    @error("uploadkan")
                        <div style="color:red; margin-top:-5%;">{{$message}}</div>
                        <br>
                    @enderror
                </div>
                <center>
                    <input type="submit" value="Add" name="add" id="add" class="btn btn-outline-primary btn-md">

        </form>
                <a href="{{url('')}}"><input type="submit" value="Input Supplier" name="supplier" id="supplier" class="btn btn-outline-primary btn-md"></a>
                <br>
                    <div class="alert" >
                        @if (Session::has('error'))
                            {{Session::get('error')}}
                        @endif
                    </div>
                </center>
        </div>
        <div class="col-md-4"></div>
    </div>
</body>
</html>



