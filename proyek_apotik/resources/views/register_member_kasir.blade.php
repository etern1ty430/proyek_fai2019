<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ URL::to('/') }}/images/registerBackground.jpg");
            background-size: cover;
            color:white;
        }
        hr {
                -moz-border-bottom-colors: none;
                -moz-border-image: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                border-color: #EEEEEE -moz-use-text-color #FFFFFF;
                border-style: solid none;
                border-width: 1px 0;
                margin: 18px 0;
            }
    </style>
</head>
<body>
<div class='row' style="width:100%;">
        <div class="col-md-3"></div>
        <div class="col-md-6" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
            <form action="{{url('kasir/processregister')}}" method="post">
            @csrf
            <hr>
            <div align="right">
                <h4>
                        Kasir : {{Session::get('user')}}
                        &nbsp;&nbsp;<input class="btn btn-danger" type="submit" value="Logout" id="logout" name="logout">
                </h4>
            </div>
            <hr>
            <div class='row' style="width:100%;">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h1>Register Customer :</h1><br>
                    <span class="label label-default">Nama Lengkap :</span>
                    <input class="form-control" type="text" name="namalengkap" id="namalengkap" style="width:100%"><br>

                    <span class="label label-default">Email :</span>
                    <input class="form-control" type="text" name="email" id="email" style="width:100%"><br>

                    <span class="label label-default">Tanggal Lahir :</span>
                    <input class="form-control" type="date" name="tgllahir" id="tgllahir"><br>

                    <span class="label label-default">Nomor Telepon :</span>
                    <input class="form-control" type="number" name="notelp" id="notelp"><br>

                    <span class="label label-default">Gender :</span>
                </div>
                <div class="col-md-2"></div>
            </div>
            <center>
                <input type="radio" name="gender" id="" value="L" style="width:5%; margin-left:-3%;" checked> <img src="/images/male.png" style="width:5%; height:5%;">
                <input type="radio" name="gender" id="" value="P" style="width:5%; margin-left:2%;"> <img src="/images/female.png" style="width:6%; height:6%;"><br><br>
                <input class="btn btn-success" type="submit" value="Register" name="register" id="register">
            </center>
            <hr>
            <center>
            <h1>Daftar Customer :</h1>
            Search : <input class="btn btn-basic" type="text" name="filter" id="filter"> &nbsp;&nbsp; <input class="btn btn-basic" type="submit" value="Find" name="find" id="find">
            <br><br>
            <table border="3 solid white" cellpadding=10>
            <tr>
                    <td>Nama Lengkap</td>
                    <td>Email : </td>
                    <td>Tanggal Lahir</td>
                    <td>Nomor Telepon</td>
                    <td>Gender</td>
                    <td>Action</td>
            </tr>
                    @foreach ($allmember as $r)
                    <tr>
                        <td>{{$r->nama}}</td>
                        <td>{{$r->email}}</td>
                        <td>{{$r->tgllhr}}</td>
                        <td>{{$r->notelp}}</td>
                        <td align="center">{{$r->gender}}</td>
                        <td><input class="btn btn-success" type="submit" value="Select" name={{$r->username}}></td>
                    </tr>
                    @endforeach
            </table>
        </center>
            <hr>
        </form>
</div>
<div class="col-md-3"></div>
</div>

</body>
</html>
