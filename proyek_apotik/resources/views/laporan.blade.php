<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ URL::to('/') }}/images/laporanBackground.jpg");
            background-size: cover;
            color:white;
        }
    </style>
</head>
<body>
<div class='row' style="width:100%;">
    <div class="col-md-2"></div>
    <div class="col-md-8" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'><center><br>
        <form action="{{url('proyek/processlaporan')}}" method="post">
            @csrf

            <select name="cbFilter" style="margin-right:5%;">
                <option value="Pembelian">Pembelian</option>
                <option value="Penjualan">Penjualan</option>
            </select>
            <input type="date" name="tglAwal" id="tglAwal" style="width:20%;"> =>
            <input type="date" name="tglSampai" id="tglSampai" style="width:20%;">

            <a href="{{url('proyek/processlaporan')}}"><button style="margin-left:1%;width:3%; height:3%; background-color:transparent;border:none;"><img src="/images/search.png" style="width:200%; height:200%;"></button></a>
            </center>

            @if (isset($h_beli))
                {{-- header report --}}
                <br><br><div class='row' style="width:100%;">
                    <div class="col-md-4"><img src="{{url('/images/iconDocmec.jpg')}}" style="margin:3% 0% 0% 50%;"></div>
                    <div class="col-md-7" style="margin-left:5%;">
                        <h2>Laporan {{$filter}} Docmec</h2>
                        <h5>Ngagel Jaya Tengah 73 - 77, Surabaya, Jawa Timur</h5>
                        <h5>Contact : 14045</h5>
                    </div>
                </div>

                {{-- isi report --}}
                <center>


                @foreach ($h_beli as $item)
                    @php
                        dump($item);
                        echo $item->nota_beli;
                    @endphp
                    <hr style="background-color:white; width:70%;">
                    <div class='row' style="width:100%;">
                        @if ($filter == "Pembelian")
                            <div class="col-md-3"></div>
                            <div class="col-md-2">Nota Beli <br> {!! $item->nota_beli !!}</div>
                            <div class="col-md-2">ID Supplier <br> {!! $item->id_supplier !!}</div>
                            <div class="col-md-2">Tanggal <br> {!! $item->tanggal !!}</div>
                            <div class="col-md-3"></div>
                        @else
                            <div class="col-md-2"></div>
                            <div class="col-md-2">Nota Jual <br> {!! $item->nota_jual !!}</div>
                            <div class="col-md-2">Customer<br> {!! $item->username_cust !!}</div>
                            <div class="col-md-2">Kasir<br> {!! $item->username_peg !!}</div>
                            <div class="col-md-2">Tanggal <br> {!! $item->tanggal !!}</div>
                            <div class="col-md-2"></div>
                        @endif


                    </div><br>
                    <table border="1px solid white" cellpadding=10>
                        <thead>
                            <tr>
                                <td>Nama Barang</td>
                                <td>Jumlah</td>
                                <td>Jenis</td>
                                <td>Harga / Pcs</td>
                                <td>Subtotal</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($d_beli as $itemDetail)
                                @php
                                    if($filter == "Pembelian") {
                                        $tempContainer = $itemDetail->nota_beli;
                                        $temp = $item->nota_beli;
                                    }
                                    else {
                                        $tempContainer = $itemDetail->nota_jual;
                                        $temp = $item->nota_jual;
                                    }
                                @endphp
                                @if ($tempContainer == $temp)
                                    <tr>
                                        <td>{{ $itemDetail->nama_barang->nama_barang }}</td>
                                        <td align="center">{{ $itemDetail->qty }}</td>
                                        <td>{{ $itemDetail->jenis }}</td>
                                        <td> Rp {{ number_format($itemDetail->harga,2,",",".") }}</td>
                                        @if ($itemDetail->jenis == "box")
                                            <td>Rp {{ number_format($itemDetail->harga*36*$itemDetail->qty,2,",",".") }}</td>
                                        @elseif ($itemDetail->jenis == "strip")
                                            <td>Rp {{ number_format($itemDetail->harga*6*$itemDetail->qty,2,",",".") }}</td>
                                        @else
                                            <td>Rp {{ number_format($itemDetail->harga*$itemDetail->qty,2,",",".") }}</td>
                                        @endif
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                    {{-- subtotal 1 pesanan --}}
                    <br><h3 style="margin-left:20%;">Total : Rp {{number_format($item->total, 2, ",",".") }}</h3>
                @endforeach
                </center>

            @endif
        </form>
    <br><br></div>
    <div class="col-md-2"></div>
</div><br><br>
</body>
</html>
