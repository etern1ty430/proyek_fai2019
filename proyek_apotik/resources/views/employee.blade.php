<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ URL::to('/') }}/images/employee_background.jpg");
            background-size: cover;
            color:white;
        }
    </style>
</head>
<body>
    <center>
    <div class='row' style="width:100%;">
    <div class="col-md-3"></div>
    <div class="col-md-6" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
        <form action="{{url('employee/process_employee')}}" method="post">
            @csrf
            <hr color="white">
            <h5 align="right">
                Employee : <span style="text-transform:capitalize;">{{Session::get('user')}}</span> &nbsp;&nbsp;
                <input type="submit" name="logout" value="Logout" class="btn btn-danger"><br>
            </h5>
            <hr color="white">
            <div class='row' style="width:100%;">
                <div class="col-md-6">
                    <input type="submit" name="laporan_barang" value="Laporan Barang" class="btn btn-success" style="width:100%;opacity:1;filter:alpha(opacity=50);background-color:rgba(40,167,69,0.5);border:0px solid black;" disabled>
                </div>
                <div class="col-md-6">
                    <input type="submit" name="laporan_barang_expired" value="Laporan Barang Expired" class="btn btn-success" style="width:100%;border:0px solid black;">
                </div>
            </div>
            <br>

            <h2>Laporan Barang</h2>
            <table border="2" cellpadding=10>
                <tr>
                    <td align="center">Gambar Obat</td>
                    <td align="center">Nama Obat</td>
                    <td align="center">Fungsi</td>
                    <td align="center">Harga</td>
                    <td align="center">Jumlah</td>
                    <td align="center">Jenis</td>
                    <td align="center">Expired</td>
                </tr>
                @if ($allbarang!=null)
                    @foreach ($allbarang as $r)
                    <tr>
                        <td align="center"><img style="width:70px; height:70px;" src="{{asset('/images/img_obat/'.$r->connect_barang->img_resource)}}" alt=""></td>
                        <td>{{$r->connect_barang->nama_barang}}</td>
                        <td>{{$r->connect_barang->fungsi}}</td>
                        <td>Rp {{number_format($r->connect_barang->harga,2,",",".")}}</td>
                        <td align="center">{{$r->stock}}</td>
                        <td>{{$r->jenis}}</td>
                        <td>{{$r->expired}}</td>
                    </tr>
                    @endforeach
                @endif
            </table>
            <br>
            <input type="submit" name="update_expired" value="Update Expired" class="btn btn-success">
            <hr color="white">
        </form>
    </div>
    <div class="col-md-3"></div>
    </div>
    </center>
</body>
</html>
