<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/jquery.min.js"></script>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.bundle.min.js"> </script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Master Barang</title>
</head>
<body class="bg-dark">
    <div class="row mt-5">
        <div class="col-8 offset-2">
            <div class="card">
                <div class="card-header bg-dark">
                    <h3 class="card-title text-white">Master Barang</h3>
                </div>
                <div class="card-body">
                    @section('pilihSupplier')

                    @show
                    <div id="tempatBarang">

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function inputSupplier() {
        var id_supplier = $('#daftar_supplier').find(":selected").val();

        $.ajax({
            type:'POST',
            url:'/masterBarang/inputSupplier',
            data: {id_supplier:id_supplier},
            success:function(data) {
                $('#tempatBarang').html(data.html);
            }
        });
    }
    function tambah(btn){
        var id_barang = $(btn).attr('id_barang');
        var jenis = $(btn).attr('jenis');
        $.ajax({
            type:'POST',
            url:'/masterBarang/tambahBarang',
            data: {id_barang:id_barang, jenis:jenis},
            success:function(data) {
                $('#tabelBarang').html(data.html);
            }
        });
    }
    function kurang(btn){
        var id_barang = $(btn).attr('id_barang');
        var jenis = $(btn).attr('jenis');
        $.ajax({
            type:'POST',
            url:'/masterBarang/kurangBarang',
            data: {id_barang:id_barang, jenis:jenis},
            success:function(data) {
                $('#tabelBarang').html(data.html);
            }
        });
    }
 </script>
