<!DOCTYPE html>
<html lang="en">
<head>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ URL::to('/') }}/images/loginBackground.jpg");
            background-size: cover;
            color:white;
        }
        hr {
                -moz-border-bottom-colors: none;
                -moz-border-image: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                border-color: #EEEEEE -moz-use-text-color #FFFFFF;
                border-style: solid none;
                border-width: 1px 0;
                margin: 18px 0;
        }
    </style>
</head>
<body>
<div class='row' style="width:100%;">
    <div class="col-md-1"></div>
    <div class="col-md-10" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
    <form action="{{url('kasir/processorder')}}" method="post">
    @csrf
    @php
        $tempdatabase=Session::get('tempdatabase');
        $cart=Session::get('cart');
        Session::put("poin_yang_dipakai",0);
        $index=0;
        $total=0;
    @endphp
    <hr>
    <div>
            <h4 align="right">
                Kasir : {{Session::get('user')}}
                &nbsp;&nbsp;<input class="btn btn-danger" type="submit" value="Logout" id="logout" name="logout">
            </h4>
    </div>
    <hr>
    @foreach ($allmember as $r)
        @if ($r->username==Session::get('member'))
            <h2>Customer : <span style="text-transform:capitalize">{{$r->nama}}</span> </h2>
        @endif
    @endforeach
    <hr>
    <center>
    <div class='row' style="width:100%;">
        {{-- bagian daftar obat --}}
        <div class="col-md-6">
            <h1>Daftar Obat : </h1>
            @php
                $index=0;
            @endphp
            <table border="3" cellpadding=13>
                <tr>
                    <td>Gambar Obat</td>
                    <td>Nama Obat</td>
                    <td>Fungsi</td>
                    <td>Harga/Pcs</td>
                    <td>Stok</td>
                    <td>Jenis</td>
                    <td>Action</td>
                </tr>
                @if ($tempdatabase!=null)
                    @foreach ($tempdatabase as $r)
                    <tr>
                        <td align="center"><img style="width:70px; height:70px;" src="{{asset('/images/img_obat/'.$r['img_resource'])}}"" alt=""></td>
                        <td>{{$r['nama_barang']}}</td>
                        <td>{{$r['fungsi']}}</td>
                        <td>{{'Rp '.number_format($r['harga'], 2, ",",".")}}</td>
                        <td>{{$r['stock']}}</td>
                        <td>{{$r['jenis']}}</td>
                        <td><input class="btn btn-success" type="submit" value="Order" name={{'order'.$index}} id={{'order'.$index}} ></td>
                    </tr>
                    @php
                        $index++;
                    @endphp
                    @endforeach
                @endif
            </table>
        </div>
        <div class="vl"></div>
        {{-- bagian cart --}}
        <div class="col-md-6">
            @php
                $index=0;
            @endphp
            <table border="3" cellpadding=13>
                @if ($cart!=null)
                    <h1>Cart : </h1>
                    <tr>
                        <td>Nama Obat</td>
                        <td>Fungsi</td>
                        <td>Harga/Pcs</td>
                        <td>Jumlah</td>
                        <td>Jenis</td>
                        <td>Action</td>
                    </tr>
                    @foreach ($cart as $r)
                    <tr>
                        <td>{{$r['nama_barang']}}</td>
                        <td>{{$r['fungsi']}}</td>
                        <td>{{'Rp '.number_format($r['harga'], 2, ",",".")}}</td>
                        <td>{{$r['stock']}}</td>
                        <td>{{$r['jenis']}}</td>
                        <td><input class="btn btn-danger" style="width:100%;font-size:18pt" type="submit" value="-" name={{'kurangi'.$index}} id={{'kurangi'.$index}}></td>
                    </tr>
                    @php
                        $index++;
                        $total=$total+($r['stock']*$r['harga']);
                    @endphp
                    @endforeach
                    @php
                        Session::put("total",$total);
                    @endphp
                @endif
        </table>
        </div>
    </div>
    <hr>
    </center>
    @foreach ($allmember as $r)
        @if ($r->username==Session::get('member'))
            <h2>Total Poin Customer : {{$r->poin.' Poin'}} </h2>
            @php
                Session::put("poin_user",$r->poin);
            @endphp
        @endif
    @endforeach
    <center>
    @if ($cart==null)
        @php
            Session::put("total",0);
        @endphp
    @endif
        <br>
        Gunakan Poin Untuk Mendapatkan Diskon : <input type="checkbox" id="myCheck" onclick="myFunction()">
        <p id="text1" style="display:none">*Catatan : 1 Poin = Diskon Rp. 2000 </p>
        <p id="text2" style="display:none">Banyaknya poin yang ingin digunakan : </p>
        <input class="btn btn-basic" type="number" id="text_isi" name="text_isi" style="display:none" onkeyup="TestOnTextChange()" onchange="TestOnTextChange()">
        <br>
        Total Harga : <input class="btn btn-basic" type="totalharga_sekarang" id="totalharga_sekarang" name="totalharga_sekarang" value="{{$total}}" readonly> <br>
        <br>
        <input class="btn btn-danger" type="submit" value="Cancel" id="cancel" name="cancel">
        <input class="btn btn-success" type="submit" value="Checkout" name="checkout" id="checkout">
    <hr>
    </form>
    </div>
</center>
    <div class="col-md-1"></div>
</div><br><br>
{{-- untuk atur poin --}}
<script>
    function myFunction() {
      var checkBox = document.getElementById("myCheck");
      var text1 = document.getElementById("text1");
      var text2 = document.getElementById("text2");
      var text_isi = document.getElementById("text_isi");
      var totalharga_sekarang = {{Session::get("total")}}; //ngawur cuy
      if(totalharga_sekarang>0){
        if (checkBox.checked == true){
          text1.style.display = "block";
          text2.style.display = "block";
          text_isi.style.display = "block";
          text_isi.style.marginLeft = "95px";
          document.getElementById('text_isi').value = 0;
        }
        else {
          text1.style.display = "none";
          text2.style.display = "none";
          text_isi.style.display = "none";
          document.getElementById('totalharga_sekarang').value = totalharga_sekarang;
          document.getElementById('text_isi').value = 0;
        }
      }
    }
    function TestOnTextChange()
    {
      var text_isi = document.getElementById("text_isi").value;
      var totalharga_sekarang = {{Session::get("total")}}; //ngawur cuy
      var poin_user = {{Session::get("poin_user")}}; //ngawur cuy
      if(text_isi!="" && text_isi<=poin_user){
        var harga_final=totalharga_sekarang-(text_isi*2000);
        if(harga_final<0){
            document.getElementById('text_isi').value = 0;
            alert("Anda Menggunakan Poin Berlebihan!");
        }
        else{
            document.getElementById('totalharga_sekarang').value = harga_final;
        }
      }
      else if(text_isi>poin_user){
        document.getElementById('totalharga_sekarang').value = totalharga_sekarang;
        document.getElementById('text_isi').value = 0;
        alert("poin Anda Tidak Cukup");
      }
      else{
        document.getElementById('totalharga_sekarang').value = totalharga_sekarang;
      }
    }
</script>
</body>
</html>


