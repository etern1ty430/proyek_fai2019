<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
    body{
        background-image: url("{{ URL::to('/') }}/images/loginBackground.jpg");
        background-size: cover;
    }
    </style>
</head>
<body>
    <br><br><br>
    <div class='row' style="width:100%;">
        <div class="col-md-4"></div>
        <div class="col-md-4" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
            <br><center>
            <div>
                <form action="{{url('proyek/processlogin')}}" method="post">
                @csrf

                <h1 style='color:white;'>LOGIN</h1>
                <br>
                <div class="form-group">
                    <label for="username" style='color:white;'>Username</label><br>
                    <center><input type="text" name="username" id="username" style="width:50%;"></center><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="password" style='color:white;'>Password</label><br>
                    <center><input type="password" name="password" id="password" style="width:50%;"></center><br><br>
                </div>

                <input type="submit" value="Forget Password" name="forgetpass" id="forgetpass" class="btn btn-outline-primary btn-md" style="width:40%; margin-top:-5%;">
                <br><br>
                <input type="submit" value="Login" name="login" id="login" class="btn btn-outline-primary btn-md" style="margin-left:33%;float:left;">
                </form>

                <a href="{{ url('proyek/pageregister')}}"><button class="btn btn-outline-primary btn-md" style="margin-left:-30%;"> Register</button></a><br><br><br>
                <div class="alert" style="margin-top:-10%">
                    @if (Session::has('error'))
                        {{Session::get('error')}}
                    @endif
                </div>
            </center>
            </div>
            <br>
        </div>
        <div class="col-md-4"></div>
    </div>
</body>
</html>

