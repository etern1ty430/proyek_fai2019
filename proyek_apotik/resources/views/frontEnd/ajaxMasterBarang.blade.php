<div>
    <hr>
    <input type="hidden" name="id_supplier" value="{{$id_supplier}}">

    <div class="form-group">
        <label for="daftarBarang">Daftar Barang</label>
        <select name="daftarBarang" id="daftarBarang" class="form-control form-control-lg" onchange="gantiHarga()">
            @foreach ($barangSupplier as $item)
                <option value="{{$item->id_barang}}" harga="{{$item->harga}}">{{$item->nama_barang}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="jumlahObat">Jumlah Obat</label>
        <input type="number" name="jumlahObat" id="jumlahObat" class="form-control" min="1">
    </div>

    <div class="form-group">
        <label for="satuanBarang">Satuan Barang</label>
        <select name="satuanBarang" id="satuanBarang" class="form-control">
            <option value="biji">Biji</option>
            <option value="strip">Strip</option>
            <option value="box">Box</option>
        </select>
    </div>

    <div class="form-group">
        <label for="harga">Harga</label>
        <p class="text-muted" id='hargaBarang'></p>
    </div>
   <button class="btn btn-primary mt-2 form-control" onclick="addToCart()">Add to cart</button>
</div>

<script>
    gantiHarga();

    function gantiHarga() {
        var harga = $('#daftarBarang').find(":selected").attr('harga');
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'IDR'
        })
        $('#hargaBarang').html('<h5>' + formatter.format(harga) + ' / biji</h5>');
        $('#hargaBarang').append('<h5>' + formatter.format(harga * 6) + ' / strip</h5>');
        $('#hargaBarang').append('<h5>' + formatter.format(harga * 36) + ' / box</h5>');
    }
    function addToCart(){
        var id_supplier = $('#id_supplier').val();
        var id_barang = $('#daftarBarang').find(":selected").val();
        var jumlah = $('#jumlahObat').val();
        var jenis = $('#satuanBarang').find(":selected").val();
        var harga = $('#daftarBarang').find(":selected").attr('harga');
        var nama = $('#daftarBarang').find(":selected").text();
        //alert(jumlah);
        $.ajax({
            type:'POST',
            url:'/masterBarang/addToCart',
            data: {id_supplier:id_supplier, jumlah:jumlah, jenis:jenis, harga:harga, id_barang:id_barang, nama:nama},
            success:function(data) {
                alert(data);
                $('#tempatBarang').html('');
            }
        });
    }
</script>

