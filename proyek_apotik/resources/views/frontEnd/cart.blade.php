@extends('masterBarang')

<?php
    $shoppingCart = session('itemCart');
?>

@section('pilihSupplier')

    <a href="/masterBarang"><button class="btn btn-danger mb-3">Kembali ke Master Barang</button></a>
    <p class="float-right mb-0">{{date("l, d F Y")}}</p>
    <div id="tabelBarang">

    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        ajaxTabel();
        function ajaxTabel(){
            $.ajax({
                type:'POST',
                url:'/masterBarang/ajaxTabel',
                success:function(data) {
                    $('#tabelBarang').html(data.html);
                }
            });
        }
    </script>
@endsection
