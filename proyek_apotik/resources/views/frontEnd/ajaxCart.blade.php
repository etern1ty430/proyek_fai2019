<?php
    $shoppingCart = session('itemCart');
    $totalBelanja = 0;
?>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>ID Barang</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Jenis</th>
            <th>Harga</th>
            <th>Subtotal</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if (isset($shoppingCart))
            @foreach ($shoppingCart as $item)
                @foreach ($item as $item2)
                    <tr>
                        <td>{{$item2['id_barang']}}</td>
                        <td>{{$item2['nama']}}</td>
                        <td>{{$item2['jumlah']}}</td>
                        <td>{{$item2['jenis']}}</td>
                        <td>Rp. {{number_format($item2['harga'])}}</td>
                        <td>Rp. {{number_format($item2['harga'] * $item2['jumlah'])}}</td>
                        @php
                            $totalBelanja += $item2['harga'] * $item2['jumlah'];
                        @endphp
                        <td>
                            <button class="btn btn-primary" style="width: 40px;" id_barang="{{$item2['id_barang']}}" jenis="{{$item2['jenis']}}" onclick="tambah(this)">+</button>
                            <button class="btn btn-primary" style="width: 40px;" id_barang="{{$item2['id_barang']}}" jenis="{{$item2['jenis']}}" onclick="kurang(this)">-</button>
                        </td>
                    </tr>
                @endforeach
            @endforeach
            @else
            <div class="alert alert-primary" role="alert">
                Cart anda masih kosong isi <a href="/masterBarang" class="alert-link">Cart</a>. Disini.
            </div>
        @endif
    </tbody>
</table>
<h3>Total Belanja : Rp. {{number_format($totalBelanja)}}</h3>
@if ($totalBelanja > 0)
    <a href="keCheckout"><button class="btn btn-success">Checkout</button></a>
@endif

