@extends('masterBarang')

@section('pilihSupplier')
    <?php
        $shoppingCart = session('itemCart');
        $totalBelanja = 0;
    ?>
    <h3>Barang yang di pesan</h3>
    <a href="cart"><button class="btn btn-danger">Kembali</button></a>
    <table class="table table-striped table-hover mt-3">
    <thead>
        <tr>
            <th>ID Barang</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Jenis</th>
            <th>Harga</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
        @if (isset($shoppingCart))
            @foreach ($shoppingCart as $item)
                @foreach ($item as $item2)
                    <tr>
                        <td>{{$item2['id_barang']}}</td>
                        <td>{{$item2['nama']}}</td>
                        <td>{{$item2['jumlah']}}</td>
                        <td>{{$item2['jenis']}}</td>
                        <td>Rp. {{number_format($item2['harga'])}}</td>
                        <td>Rp. {{number_format($item2['harga'] * $item2['jumlah'])}}</td>
                        @php
                            $totalBelanja += $item2['harga'] * $item2['jumlah'];
                        @endphp
                    </tr>
                @endforeach
            @endforeach
        @endif
    </tbody>
    </table>
    <h3>Total Belanja : Rp. {{number_format($totalBelanja)}}</h3>
    <a href="checkOut"><button class="btn btn-success">Pesan</button></a>
@endsection
