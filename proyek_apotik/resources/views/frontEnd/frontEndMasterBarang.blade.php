@extends('masterBarang')

@section('pilihSupplier')
    <form action="{{url('masterBarang/inputSupplier')}}" method="post">
        @csrf

    </form>
    <div class="form-group">
        <label for="daftar_supplier">Daftar Supplier</label>
        <select class="form-control form-control-lg" name="id_supplier" id="daftar_supplier">
            @foreach ($temp as $item)
                <option value="{{$item->id_supplier}}">{{$item->nama_supplier}}</option>
            @endforeach
        </select>
        <button class="btn btn-primary form-control mt-2" onclick="inputSupplier()">Pilih</button>
        <hr>
        <a href="masterBarang/cart" class="btn btn-success form-control">TO CART</a>
    </div>
@endsection

