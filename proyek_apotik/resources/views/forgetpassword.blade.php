<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="/bootstrap/css/bootstrap.min.css"> </script>
    <title>Document</title>
    <style>
    body{
        background-image: url("{{ URL::to('/') }}/images/forgetBackground.jpg");
        background-size: cover;
        color:white;
    }
    </style>
</head>
<body>
    <br>
    <div class='row' style="width:100%;">
        <div class="col-md-4"></div>
        <div class="col-md-4" style='border-radius:4px; 2px;opacity:0.8;background:black;margin-top:4%;'>
        <form action="{{url('proyek/processforgot')}}" method="post">
            @csrf
            <br>
            <center>
            <div>
                <h1 style='color:white;'>FORGET PASSWORD</h1>
                <br>
                <div class="form-group">
                    <label for="username" style='color:white;'>Username</label><br>
                    <center><input type="text" name="username" id="username" style="width:50%;"></center><br>
                </div>
                <div class="form-group" style="margin-top:-5%;">
                    <label for="oldpassword" style='color:white;'>Old Password</label><br>
                    <center><input type="text" name="oldpassword" id="oldpassword" style="width:50%;"></center><br><br>
                </div>
                <div class="form-group" style="margin-top:-10%;">
                    <label for="password" style='color:white;'>New Password</label><br>
                    <center><input type="text" name="password" id="password" style="width:50%;"></center><br><br>
                </div>
                <div class="form-group" style="margin-top:-10%;">
                    <label for="confpass" style='color:white;'>Confirm Password</label><br>
                    <center><input type="text" name="confpass" id="confpass" style="width:50%;"></center><br>
                </div>
                <a href="{{url('proyek/processforgot')}}"><button class="btn btn-outline-primary btn-md"> Change</button></a>
            </form>
            <form action="{{url('')}}" method="get">
                <br><a href="{{url('')}}"><button class="btn btn-outline-primary btn-md"> Back to Login</button></a>
            </form>
                <div class="alert">
                    @if (Session::has('error'))
                        {{Session::get('error')}}
                    @endif
                </div>
                <br>
            </center>
            </div> <br>
        </div>
        <div class="col-md-4"></div>
    </div>
</body>
</html>

