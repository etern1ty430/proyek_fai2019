-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2020 at 01:22 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `docmec`
--
CREATE DATABASE IF NOT EXISTS `docmec` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `docmec`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tgllhr` date NOT NULL,
  `notelp` varchar(255) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `status` varchar(1) NOT NULL,
  `poin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`username`, `password`, `nama`, `tgllhr`, `notelp`, `gender`, `status`, `poin`) VALUES
('asd', '123', 'hai', '2019-11-01', '12345678', 'L', 'E', 0),
('elzzer', '234', 'ferd', '1999-03-04', '12345678', 'L', 'C', 0),
('member0', '', 'richard', '2019-11-04', '08123546798', 'L', 'M', 1),
('xeld', '321', 'richard', '2019-11-01', '08123445', 'L', 'E', 0);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang` (
  `id_barang` varchar(255) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `fungsi` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `img_resource` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `fungsi`, `harga`, `img_resource`) VALUES
('BA001', 'Panadol Demam', 'sakit kepala', 5000, 'panadol demam.png'),
('BA002', 'Diapet', 'sembelit', 3000, 'diapet.png'),
('BA003', 'Antangin', 'sakit kepala', 10000, 'antangin.png'),
('BA004', 'Panadol Batuk', 'batuk', 20000, 'panadol batuk.png'),
('BA005', 'Fix Formula 44', 'batuk berdahak', 28000, 'fix formula 44.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `barang_supplier`
--

DROP TABLE IF EXISTS `barang_supplier`;
CREATE TABLE `barang_supplier` (
  `id_barangsupplier` int(11) NOT NULL,
  `id_supplier` varchar(255) NOT NULL,
  `id_barang` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_supplier`
--

INSERT INTO `barang_supplier` (`id_barangsupplier`, `id_supplier`, `id_barang`, `harga`) VALUES
(1, 'SU001', 'BA001', 3000),
(2, 'SU002', 'BA002', 1500),
(3, 'SU001', 'BA003', 7500),
(5, 'SU001', 'BA004', 15000),
(6, 'SU003', 'BA005', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `d_barang`
--

DROP TABLE IF EXISTS `d_barang`;
CREATE TABLE `d_barang` (
  `id_dbarang` int(11) NOT NULL,
  `id_barang` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `expired` date NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_barang`
--

INSERT INTO `d_barang` (`id_dbarang`, `id_barang`, `stock`, `expired`, `jenis`, `status`) VALUES
(1, 'BA001', 5, '2019-12-01', 'strip', 0),
(2, 'BA001', 5, '2019-12-01', 'box', 0),
(3, 'BA001', 5, '2025-01-02', 'biji', 1);

-- --------------------------------------------------------

--
-- Table structure for table `d_beli`
--

DROP TABLE IF EXISTS `d_beli`;
CREATE TABLE `d_beli` (
  `id_dbeli` int(11) NOT NULL,
  `nota_beli` varchar(255) NOT NULL,
  `id_barang` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_beli`
--

INSERT INTO `d_beli` (`id_dbeli`, `nota_beli`, `id_barang`, `qty`, `jenis`, `harga`) VALUES
(1, 'NB001', 'BA001', 2, 'biji', 3000),
(2, 'NB002', 'BA002', 3, 'biji', 1500),
(3, 'NB001', 'BA003', 1, 'strip', 7500);

-- --------------------------------------------------------

--
-- Table structure for table `d_jual`
--

DROP TABLE IF EXISTS `d_jual`;
CREATE TABLE `d_jual` (
  `id_djual` int(11) NOT NULL,
  `nota_jual` varchar(255) NOT NULL,
  `id_barang` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_jual`
--

INSERT INTO `d_jual` (`id_djual`, `nota_jual`, `id_barang`, `qty`, `jenis`, `harga`) VALUES
(1, 'NJ001', 'BA003', 2, 'biji', 10000),
(2, 'NJ001', 'BA002', 1, 'biji', 3000),
(3, 'NJ002', 'BA001', 2, 'biji', 5000),
(4, 'NJ003', 'BA001', 3, 'biji', 5000),
(5, 'NJ004', 'BA001', 3, 'biji', 5000),
(6, 'NJ005', 'BA001', 3, 'biji', 5000),
(7, 'NJ006', 'BA001', 3, 'biji', 5000),
(8, 'NJ007', 'BA001', 3, 'biji', 5000),
(9, 'NJ008', 'BA001', 3, 'biji', 5000),
(10, 'NJ009', 'BA001', 3, 'biji', 5000),
(11, 'NJ010', 'BA001', 3, 'biji', 5000),
(12, 'NJ011', 'BA001', 3, 'biji', 5000),
(13, 'NJ012', 'BA001', 3, 'biji', 5000),
(14, 'NJ013', 'BA001', 3, 'biji', 5000),
(15, 'NJ014', 'BA001', 3, 'biji', 5000),
(16, 'NJ015', 'BA001', 3, 'biji', 5000),
(17, 'NJ016', 'BA001', 3, 'biji', 5000),
(18, 'NJ017', 'BA001', 3, 'biji', 5000),
(19, 'NJ018', 'BA001', 3, 'biji', 5000),
(20, 'NJ019', 'BA001', 3, 'biji', 5000),
(21, 'NJ020', 'BA001', 3, 'biji', 5000),
(22, 'NJ021', 'BA001', 3, 'biji', 5000),
(23, 'NJ022', 'BA001', 3, 'biji', 5000),
(24, 'NJ023', 'BA001', 3, 'biji', 5000),
(25, 'NJ024', 'BA001', 3, 'biji', 5000),
(26, 'NJ025', 'BA001', 3, 'biji', 5000),
(27, 'NJ026', 'BA001', 3, 'biji', 5000),
(28, 'NJ027', 'BA001', 3, 'biji', 5000),
(29, 'NJ028', 'BA001', 3, 'biji', 5000),
(30, 'NJ029', 'BA001', 3, 'biji', 5000),
(31, 'NJ030', 'BA001', 3, 'biji', 5000),
(32, 'NJ031', 'BA001', 3, 'biji', 5000),
(33, 'NJ032', 'BA001', 3, 'biji', 5000),
(34, 'NJ033', 'BA001', 3, 'biji', 5000),
(35, 'NJ034', 'BA001', 3, 'biji', 5000),
(36, 'NJ035', 'BA001', 3, 'biji', 5000),
(37, 'NJ036', 'BA001', 3, 'biji', 5000),
(38, 'NJ037', 'BA001', 3, 'biji', 5000),
(39, 'NJ038', 'BA001', 3, 'biji', 5000),
(40, 'NJ039', 'BA001', 3, 'biji', 5000),
(41, 'NJ040', 'BA001', 3, 'biji', 5000),
(42, 'NJ041', 'BA001', 3, 'biji', 5000),
(43, 'NJ042', 'BA001', 3, 'biji', 5000),
(44, 'NJ043', 'BA001', 3, 'biji', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `h_beli`
--

DROP TABLE IF EXISTS `h_beli`;
CREATE TABLE `h_beli` (
  `nota_beli` varchar(255) NOT NULL,
  `id_supplier` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_beli`
--

INSERT INTO `h_beli` (`nota_beli`, `id_supplier`, `tanggal`, `total`) VALUES
('NB001', 'SU001', '2019-11-01', 51000),
('NB002', 'SU002', '2019-11-02', 4500);

-- --------------------------------------------------------

--
-- Table structure for table `h_jual`
--

DROP TABLE IF EXISTS `h_jual`;
CREATE TABLE `h_jual` (
  `nota_jual` varchar(255) NOT NULL,
  `username_cust` varchar(255) NOT NULL,
  `username_peg` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_jual`
--

INSERT INTO `h_jual` (`nota_jual`, `username_cust`, `username_peg`, `tanggal`, `total`) VALUES
('NJ001', 'member0', 'elzzer', '2019-11-01', 23000),
('NJ002', 'member0', 'elzzer', '2020-01-02', 10000),
('NJ003', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ004', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ005', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ006', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ007', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ008', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ009', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ010', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ011', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ012', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ013', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ014', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ015', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ016', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ017', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ018', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ019', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ020', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ021', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ022', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ023', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ024', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ025', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ026', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ027', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ028', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ029', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ030', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ031', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ032', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ033', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ034', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ035', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ036', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ037', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ038', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ039', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ040', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ041', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ042', 'member0', 'elzzer', '2020-01-02', 15000),
('NJ043', 'member0', 'elzzer', '2020-01-02', 15000);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id_supplier` varchar(255) NOT NULL,
  `nama_supplier` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `notelp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `notelp`) VALUES
('SU001', 'Pabrik 1', 'jln mangga 1', '0812345678'),
('SU002', 'Pabrik 2', 'jln semangka no 2', '08187654321'),
('SU003', 'pabrik3', 'jln strawberry 10', '081192837465');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `barang_supplier`
--
ALTER TABLE `barang_supplier`
  ADD PRIMARY KEY (`id_barangsupplier`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `d_barang`
--
ALTER TABLE `d_barang`
  ADD PRIMARY KEY (`id_dbarang`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `d_beli`
--
ALTER TABLE `d_beli`
  ADD PRIMARY KEY (`id_dbeli`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `nota_beli` (`nota_beli`);

--
-- Indexes for table `d_jual`
--
ALTER TABLE `d_jual`
  ADD PRIMARY KEY (`id_djual`),
  ADD KEY `nota_jual` (`nota_jual`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `h_beli`
--
ALTER TABLE `h_beli`
  ADD PRIMARY KEY (`nota_beli`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `h_jual`
--
ALTER TABLE `h_jual`
  ADD PRIMARY KEY (`nota_jual`),
  ADD KEY `username_cust` (`username_cust`),
  ADD KEY `username_peg` (`username_peg`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_supplier`
--
ALTER TABLE `barang_supplier`
  MODIFY `id_barangsupplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `d_barang`
--
ALTER TABLE `d_barang`
  MODIFY `id_dbarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `d_beli`
--
ALTER TABLE `d_beli`
  MODIFY `id_dbeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `d_jual`
--
ALTER TABLE `d_jual`
  MODIFY `id_djual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang_supplier`
--
ALTER TABLE `barang_supplier`
  ADD CONSTRAINT `barang_supplier_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`),
  ADD CONSTRAINT `barang_supplier_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`);

--
-- Constraints for table `d_barang`
--
ALTER TABLE `d_barang`
  ADD CONSTRAINT `d_barang_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`);

--
-- Constraints for table `d_beli`
--
ALTER TABLE `d_beli`
  ADD CONSTRAINT `d_beli_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`),
  ADD CONSTRAINT `d_beli_ibfk_2` FOREIGN KEY (`nota_beli`) REFERENCES `h_beli` (`nota_beli`);

--
-- Constraints for table `d_jual`
--
ALTER TABLE `d_jual`
  ADD CONSTRAINT `d_jual_ibfk_1` FOREIGN KEY (`nota_jual`) REFERENCES `h_jual` (`nota_jual`),
  ADD CONSTRAINT `d_jual_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`);

--
-- Constraints for table `h_beli`
--
ALTER TABLE `h_beli`
  ADD CONSTRAINT `h_beli_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`);

--
-- Constraints for table `h_jual`
--
ALTER TABLE `h_jual`
  ADD CONSTRAINT `h_jual_ibfk_1` FOREIGN KEY (`username_cust`) REFERENCES `account` (`username`),
  ADD CONSTRAINT `h_jual_ibfk_2` FOREIGN KEY (`username_peg`) REFERENCES `account` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
